class My_list(list):
    def __init__(self, froggit):
        for i in froggit:
            self.append(i)
    def __sub__(self, other):
        if len(self) < len(other):
            shy_froggit = other.copy()
            for i in range(len(self)):
                shy_froggit[i] = self[i] - other[i]
            for i in range(len(self), len(other)):
                shy_froggit[i] = -other[i]
        else:
            shy_froggit = self.copy()
            for i in range(len(other)):
                shy_froggit[i] = self[i] - other[i]
            for i in range(len(other), len(self)):
                shy_froggit[i] = -self[i]
        return My_list(shy_froggit)
    def __truediv__(self, divider):
        strange_froggit = self.copy()
        for i in range(len(self)):
            strange_froggit[i] = self[i] / divider
        return My_list(strange_froggit)
    def __str__(self):
        return f'{super().__str__()} its me'

angry_froggit = My_list((1, 2, 3))
kind_froggit = My_list((3, 4, 5))
gracious_froggit = kind_froggit - angry_froggit
awful_froggit = angry_froggit - kind_froggit
print(gracious_froggit)
print(awful_froggit)
print(angry_froggit / 5)