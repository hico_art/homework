import math

class Point():
    def __init__(self, x, y):
        self._x = x
        self._y = y

class Triangle():
    def __init__(self, a, b, c):
        self._a = a
        self._b = b
        self._c = c
        self.count_s()
    def count_s(self):
        len_ac = math.sqrt((self._a._x - self._c._x)**2 + (self._a._y - self._c._y)**2)
        len_bc = math.sqrt((self._b._x - self._c._x)**2 + (self._b._y - self._c._y)**2)
        len_ab = math.sqrt((self._a._x - self._b._x)**2 + (self._a._y - self._b._y)**2)
        p = (len_ab + len_ac + len_bc) / 2
        s = math.sqrt(abs(p*(p-len_ab)*(p-len_ac)*(p-len_bc)))
        self._s = s
    def __str__(self):
        return str(self._s)
    def __gt__(self, other): #>
        if self._s > other._s:
            return True
        else: return False
    def __lt__(self, other): #<
        if self._s < other._s:
            return True
        else: return False
    def __eq__(self, other): #==
        if self._s == other._s:
            return True
        else: return False
    def __ne__(self, other): #!=
        if self._s != other._s:
            return True
        else: return False
    def __le__(self, other): #<=
        if self._s <= other._s:
            return True
        else: return False
    def __ge__(self, other): #>=
        if self._s >= other._s:
            return True
        else: return False