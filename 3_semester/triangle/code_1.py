import math
from classes import Point, Triangle

points = open('plist.txt').readline().replace(' ', '').replace('[', '').split(']')[:-1]

x = []
y = []
triangles = []

for i in range(len(points)):
    x.append(int(points[i].split(',')[0]))
    y.append(int(points[i].split(',')[1]))

for a in range(0, 100):
    for b in range(a, 100):
        for c in range(b, 100):
            len_ac = math.sqrt((x[a] - x[c])**2 + (y[a] - y[c])**2)
            len_bc = math.sqrt((x[b] - x[c])**2 + (y[b] - y[c])**2)
            len_ab = math.sqrt((x[a] - x[b])**2 + (y[a] - y[b])**2)
            if len_ab != len_ac and len_ab != len_bc and len_ac != len_bc:
                if (len_ab + len_bc > len_ac) and (len_ac + len_bc > len_ab) and (len_ac + len_ab > len_bc):
                    triangles.append(Triangle(Point(x[a], y[a]), Point(x[b], y[b]), Point(x[c], y[c])))
                    
print(max(triangles))
print(min(triangles))