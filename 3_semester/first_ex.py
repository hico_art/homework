import random
import operator
import time
from tkinter import ttk
from tkinter import *

def task1():
    a, b, c = 5, 11, 7
    b, c, a = c, a, b
    print(a, b, c)
    list_ex()

def task2():
    try:
        n = int(input('Введите количество вводимых данных: '))
        summ = 0
        for i in range(1, n+1):
            a = float(input('Введите число: '))
            summ+=a
        print(int(summ))
        list_ex()
    except ValueError:
        print('Попробуйте ввести число снова')
        task2()

def task3():
    x = float(input('Введите число от 0 до 100: '))
    if x >= 0 and x <= 100:
        print(int(x**5))
        print(int(x*x*x*x*x))
        list_ex()
    else:
        print('Число не из указанного промежутка.')
        task3()

def task4():  
    x = int(input('Введите число из промежутка от 0 до 250: '))
    f1 = 0
    f2 = 1
    isTrue = False
    if x<0 and x>250:
        print('Повторите ввод.')
        task4()
    else:
        while f2 < 250+1:
            if x == f2 or x == 0:
                print('Это число Фибоначчи.')
                list_ex()
                isTrue = True
                break
            f1, f2 = f2, f1+f2
        if isTrue==False:
            print('Это не число Фибоначчи.')
            task4()

def task5():
    x = int(input('Введите номер месяца: '))
    winter = [1, 2]
    spring = [3, 4, 5]
    summer = [6, 7, 8]
    autumn = [9, 10, 11]
    winter_re = [12]
    if x >= 1 and  x <= 12:
        for i in range(0, 2+1):
            if winter[i] == x:
                print('Зима')
                list_ex()
            elif spring[i] == x:
                print('Весна')
                list_ex()
            elif summer[i] == x:
                print('Лето')
                list_ex()
            elif autumn[i] == x:
                print('Осень')
                list_ex()
            elif winter_re[i] == x:
                print('Зима')
                list_ex()
    else:
        print('Число не соответствует требованию.')
        task5()

def task6():
    try:
        N = int(input('Введите целое число: '))
        summ = 0
        even = 0
        uneven = 0
        for i in range(1, N+1):
            summ+=1
            if i%2==0:
                even+=1
            else:
                uneven+=1
        print(f'Сумма: {summ}, количество чётных: {even}, количество нечётных: {uneven}.')
        list_ex()
    except ValueError:
        print('Введите именно целое число.')
        task6()

def task7():
    try:
        N = int(input('Введите число, меньше 250: '))
        count = 0
        if N < 250:
            for i in range(1, N+1):
                for j in range(1, (N//2)+1):
                    if i%j==0 and i!=j:
                        count+=1
                print(i, count)
                count = 0
        else:
            print('Число не подходит условию.')
            task7()
    except ValueError:
        print('Вы ввели не то, что требовалось. Повторите ввод.') 
        task7()
    list_ex()

def task8():
    n, m = int(input('Первое число: ')), int(input('Второе число: '))
    piph = []
    for i in range(n, m+1):
        for j in range(n, m+1):
            for k in range(n, m+1):
                if i**2 + j**2 == k**2:
                    piph.append((i, j, k))
    print(piph)
    list_ex()

def task9():
    n, m = int(input('Первое число: ')), int(input('Второе число: '))
    for i in range(n, m+1):
        if all(delet and not i % delet for delet in (int(j) for j in str(i))):
            print(i, end = ' ')
            list_ex()

def task10():
    N = int(input('Введите целое число, меньшее 5: '))
    summ = 0
    count = 0
    i = 1
    if N >= 5:
        print('Повторите ввод числа.')
        task10()
    else:
        while count < N:
            for j in range(1, i//2+1):
                if i%j==0:
                    summ+=j
            if summ==i:
                count+=1
                print(i)
            summ = 0
            i+=1
        list_ex() 

def task11():
    array = [random.randint(1, 10) for i in range(5)]
    work_time = time.time()
    print(array)
    print(array[-1], time.time()-work_time)
    array_re = operator.itemgetter(-1)(array)
    print(array_re, time.time()-work_time)
    print(array.pop(), time.time()-work_time)
    list_ex()

def task12():
    array_for_twelve = [random.randint(1, 100) for i in range(20)]
    print(array_for_twelve)
    array_for_twelve.reverse()
    print(array_for_twelve)
    list_ex()

def task13():
    array_friday = [random.randint(1, 100) for i in range(10)]
    def sum_elem_arr(summ, index):
        if index < len(array_friday):
            return sum_elem_arr(summ+array_friday[index], index+1)
        else:
            return summ
    print(array_friday)
    print(sum_elem_arr(0, 0))
    list_ex()

def task14():
    window = Tk()
    window.title('Конвертер')
    frame = Frame(
        window,
        padx = 10,
        pady = 10,
    )
    frame.pack(expand=True)
    
    rubles = Label(
        frame,
        text = 'RUB',
    )
    rubles.grid(row=1)

    def conv():
        R = int(rubles_ent.get())
        D['text'] = round(R/97.0875, 4)

    rubles_ent = Entry(
        frame,
    )
    rubles_ent.grid(row=1, column=2)

    dollars = Label(
        frame,
        text = 'USD',
    )
    dollars.grid(row=2)

    D = Label(
        frame,
        text = '',
    )
    D.grid(row=2, column=2)

    converter = Button(
        frame,
        text = "Convert",
        command = conv,
    )
    converter.grid(row=3, column=2)

    def reverse():
        dollars = Label(
        frame,
        text = 'USD',
        )
        dollars.grid(row=1)

        def conv():
            D = int(dollars_ent.get())
            R['text'] = round(D*97.057, 3)

        dollars_ent = Entry(
            frame,
        )
        dollars_ent.grid(row=1, column=2)

        rubles = Label(
            frame,
            text = 'RUB',
        )
        rubles.grid(row=2)

        R = Label(
            frame,
            text = '',
        )
        R.grid(row=2, column=2)

        converter = Button(
            frame,
            text = "Convert",
            command = conv,
        )
        converter.grid(row=3, column=2)
        
    reverse_con = Button(
        frame,
        text = "Change on USD",
        command = reverse,
    )  
    reverse_con.grid(row=4, column=2)  

    def reverse_again():
        rubles = Label(
        frame,
        text = 'RUB',
        )
        rubles.grid(row=1)

        def conv():
            R = int(rubles_ent.get())
            D['text'] = round(R/97.0875, 4)

        rubles_ent = Entry(
            frame,
        )
        rubles_ent.grid(row=1, column=2)

        dollars = Label(
            frame,
            text = 'USD',
        )
        dollars.grid(row=2)

        D = Label(
            frame,
            text = '',
        )
        D.grid(row=2, column=2)

        converter = Button(
            frame,
            text = "Convert",
            command = conv,
        )
        converter.grid(row=3, column=2)
    
    reverse_cont = Button(
        frame,
        text = "Change on RUB",
        command = reverse_again,
        )   
    reverse_cont.grid(row=5, column=2)
    window.mainloop()
    list_ex()

def task15():
    n, m = int(input('Введите ширину (число от 5 до 20): ')), int(input('Введите высоту (число от 5 до 20): '))
    li = [0 for k in range(n)] 
    for i in range(1, m+1):
        for j in range(1, n+1):
            li[j-1] = i*j
        print(li)
    list_ex()

def task16():
       print('В разработке. Извините.')
       list_ex()

def list_ex():
    file = open('list_ex.txt', encoding='utf-8').readlines()
    for i in range(0, len(file)):
        print(file[i])
    exercise = int(input('Введите номер задания (прим.: 1): '))
    match exercise:
        case 1:
            task1()
        case 2:
            task2()
        case 3:
            task3()
        case 4:
            task4()
        case 5:
            task5()
        case 6:
            task6()
        case 7:
            task7()
        case 8:
            task8()
        case 9:
            task9()
        case 10:
            task10()
        case 11:
            task11()
        case 12:
            task12()
        case 13:
            task13()
        case 14:
            task14()
        case 15:
            task15()
        case 16:
            task16()

list_ex()