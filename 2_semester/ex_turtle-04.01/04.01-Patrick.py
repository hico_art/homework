import turtle

pen = turtle.Turtle()
pen.speed(5)
pen.width(3)
screen = turtle.Screen()
screen.screensize(400,300)
#turtle.tracer(0)

for i in range(360):
    pen.fd(1)
    pen.left(1)
    
pen.up()
pen.goto(45, 90)
pen.down()
pen.left(60)

for i in range(295):
    pen.fd(1)
    pen.right(1)



pen.up()
pen.goto(100, 120)
pen.down()
pen.right(20)
pen.fd(140)

for i in range(155):
    pen.fd(1.25)
    pen.left(1)

pen.fd(600)
pen.up()
pen.goto(130, 15)
pen.down()
pen.left(25)
pen.fd(350)

pen.up()
pen.goto(190, -310)
pen.right(185)
pen.down()
for i in range(0,90):
    pen.fd(2.25)
    pen.left(0.2)
for i in range(0,90):
    pen.fd(1)
    pen.left(0.75)
for i in range(0,90):
    pen.fd(1)
    pen.left(0.5)
for i in range(0,105):
    pen.fd(2)
    pen.left(0.35)
pen.left(80)
for i in range(148):
    pen.fd(2)
    pen.left(0.175)

pen.up()
pen.goto(10,50)
pen.down()
pen.begin_fill()
for i in range(360):
    pen.fd(0.1)
    pen.left(1)
pen.end_fill()

pen.up()
pen.goto(110, 55)
pen.down()
pen.begin_fill()
for i in range(360):
    pen.fd(0.1)
    pen.left(1)
pen.end_fill()

