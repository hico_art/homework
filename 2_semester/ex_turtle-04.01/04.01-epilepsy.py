import turtle

screen = turtle.Screen()
turtle.bgcolor("black")
screen.tracer(0)
def spiral():
    colors = ["red", "blue", "green", "yellow", "orange", "pink"]
    turtle.pensize(5)
    turtle.speed(0)
    for i in range(360):
        turtle.pencolor(colors[i%len(colors)])
        turtle.pensize(i/50)
        turtle.fd(i)
        turtle.left(59)

while True:
    spiral()
    turtle.update()
    turtle.clear()
    turtle.up()
    turtle.goto(0,0)
    turtle.down()
    turtle.left(10)





