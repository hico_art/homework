from tkinter import ttk
from tkinter import *

window = Tk()
window.title("Самый простой тест")
window.geometry("400x500")
qs_ans = open("test.txt", encoding="utf-8").readlines()
rg_ans = []
for j in range(0, len(qs_ans)-1):
    if qs_ans[j][0] == "+":
        rg_ans.append(qs_ans[j][1::])
        qs_ans[j] = qs_ans[j][1::]
answer = "1"
i = -1
score = 0
def start():
    global qs_ans
    global i 
    i+=1
    global start_text
    global answer
    if i == 5:
        global score
        if answer == rg_ans[i-1]:
            score += 1
        answer = ""
        start_text.config(text="Pезультат: " + str(score))
    elif answer != "":
        start_text.config(text=qs_ans[i*5])
        global start_button
        start_button.config(text="Ответить")
        start_button.grid(row=6)
        global answer1
        answer1.grid( row=2)
        global answer2
        answer2.grid( row=3)
        global answer3
        answer3.grid( row=4)
        global answer4
        answer4.grid( row=5)
        answer1.config(text=qs_ans[i*5+1])
        answer2.config(text=qs_ans[i*5+2])
        answer3.config(text=qs_ans[i*5+3])
        answer4.config(text=qs_ans[i*5+4])
        if i > 0:
            if answer == rg_ans[i-1]:
                score += 1
            answer = ""
def a1():
    global answer
    global i
    answer = qs_ans[i*5+1]
    print(rg_ans[i])
def a2():
    global answer 
    global i
    answer = qs_ans[i*5+2]
    print(rg_ans[i])
def a3():
    global answer 
    global i
    answer = qs_ans[i*5+3]
    print(rg_ans[i])
def a4():
    global i
    global answer 
    answer = qs_ans[i*5+4]
    print(rg_ans[i])


frame = Frame(
    window,
    padx = 10,
    pady = 10,
)
frame.pack(expand=True)

start_text = Label(
    frame,
    text = "Самый легкий тест",
)
start_text.grid(row=1)

start_button = Button(
    frame,
    text = "Начать",
    command = start,
)
start_button.grid(row=2)

answer1 = Button(
    frame,
    text = qs_ans[1],
    command=a1
)
answer2 = Button(
    frame,
    text = qs_ans[2],
    command=a2
)
answer3 = Button(
    frame,
    text = qs_ans[3],
    command=a3
)
answer4 = Button(
    frame,
    text = qs_ans[4],
    command=a4
)
window.mainloop()