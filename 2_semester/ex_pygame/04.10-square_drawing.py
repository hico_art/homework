import pygame
pygame.init()

clock = pygame.time.Clock()
screen = pygame.display.set_mode([300, 300])
WHITE = (255, 255, 255)
screen.fill(WHITE)
colors = ["red", "blue", "green", "yellow", "pink", "orange", "brown"]
i = 0
v = 2.5
x, y = 145, 145
square_children = []
running = True
while running:
    clock.tick(30)
    pygame.display.flip()
    screen.fill(WHITE)
    pygame.draw.rect(screen, colors[i], (x, y, 10, 10), 0)
    for j in range(0, len(square_children)):
        pygame.draw.rect(screen, colors[square_children[j][0]], (square_children[j][1]), 0)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_c:
                if i == 6:
                    i = 0
                else:
                    i += 1
        if event.type == pygame.QUIT:
            running = False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_w]:
        y -= v
        square_children.append([i, (x, y, 10, 10)])
    elif keys[pygame.K_s]:
        y += v
        square_children.append([i, (x, y, 10, 10)])
    if keys[pygame.K_a]:
        x -= v
        square_children.append([i, (x, y, 10, 10)])
    elif keys[pygame.K_d]:
        x += v
        square_children.append([i, (x, y, 10, 10)])
    if y <= 0:
        y += v
    elif y >= 290:
        y -= v
    elif x <= 0:
        x += v
    elif x >= 290:
        x -= v 