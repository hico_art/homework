import pygame as pg
pg.init()

screen = pg.display.set_mode([500, 500])
running = True
while running:
    for event in pg.event.get():
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_RETURN or event.key == pg.K_SPACE or event.key == pg.K_ESCAPE or \
                    event.key == pg.K_DOWN or event.key == pg.K_UP or event.key == pg.K_RIGHT or \
                    event.key == pg.K_LEFT or event.key == pg.K_w or event.key == pg.K_s or \
                    event.key == pg.K_a or event.key == pg.K_d:
                print(pg.key.name(event.key))
        if event.type == pg.QUIT:
            running = False

