from tkinter import ttk
from tkinter import *

window = Tk()
window.title("Сложение двух чисел")

frame = Frame(
    window,
    padx = 10,
    pady = 10
)
frame.pack(expand=True)

first_number = Label(
    frame,
    text = "Первое число",
)
first_number.grid(row=1, column=1)

second_number = Label(
    frame,
    text = "Второе число",
)
second_number.grid(row=2, column=1)

first_number = Entry(
    frame,
)
first_number.grid(row=1, column=2)

second_number = Entry(
    frame,
)
second_number.grid(row=2, column=2)

def sum_num():
    f_ = int(first_number.get())
    s_ = int(second_number.get())
    answer["text"] = f_ + s_
    
sum_number = Button(
    frame,
    text = "Сумма",
    command = sum_num,
)
sum_number.grid(row=3, column=2)

answer_1 = Label(
    frame,
    text = "Ответ",
)
answer_1.grid(row=4, column=1)
answer = Label(
    frame,
    text = "",
)
answer.grid(row=4, column=2)


window.mainloop()


