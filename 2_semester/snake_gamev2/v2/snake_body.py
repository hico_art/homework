import pygame as pg


class Body(pg.sprite.Sprite):
    def __init__(self, sprite, x, y, vec, speed):
        self.sprite = pg.image.load(sprite)
        self.rect = self.sprite.get_rect()
        self.rect.move_ip(x, y)
        self.x = x
        self.y = y
        self.vec = vec
        self.speed = speed
        if vec == [0, 1]:
            self.rect.move_ip(-self.speed, 0)
            self.spritedrawn = self.sprite
        if vec == [0, -1]:
            self.rect.move_ip(self.speed, 0)
            self.spritedrawn = pg.transform.rotate(self.sprite, 180)
        if vec == [-1, 0]:
            self.rect.move_ip(0, -self.speed)
            self.spritedrawn = pg.transform.rotate(self.sprite, -90)
        if vec == [1, 0]:
            self.rect.move_ip(0, self.speed)
            self.spritedrawn = pg.transform.rotate(self.sprite, 90)

    def move(self, vec, x, y):
        self.vec = vec
        self.rect.move_ip(-self.x, -self.y)
        self.rect.move_ip(x, y)
        self.x = x
        self.y = y
        if vec == [0, 1]:
            self.spritedrawn = self.sprite
        if vec == [0, -1]:
            self.spritedrawn = pg.transform.rotate(self.sprite, 180)
        if vec == [-1, 0]:
            self.spritedrawn = pg.transform.rotate(self.sprite, -90)
        if vec == [1, 0]:
            self.spritedrawn = pg.transform.rotate(self.sprite, 90)
