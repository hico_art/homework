import pygame as pg


class Apple(pg.sprite.Sprite):
    def __init__(self, sprite, startx, starty):
        self.sprite = pg.image.load(sprite)
        self.rect = self.sprite.get_rect()
        self.rect.move_ip(startx, starty)
        self.x = startx
        self.y = starty

    def teleport(self, c_x, c_y):
        self.rect.move_ip(-self.x, -self.y)
        self.rect.move_ip(c_x, c_y)
        self.x = c_x
        self.y = c_y
