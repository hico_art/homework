import pygame as pg


class Snake(pg.sprite.Sprite):
    def __init__(self, sprite, startx, starty, vec, speed):
        self.sprite = pg.image.load(sprite)
        self.rect = self.sprite.get_rect()
        self.rect.move_ip(startx, starty)
        self.vec = vec
        self.speed = speed
        self.x = startx
        self.y = starty
        if vec == [0, 1]:
            self.rect.move_ip(-self.speed, 0)
            self.spritedrawn = self.sprite
        if vec == [0, -1]:
            self.rect.move_ip(self.speed, 0)
            self.spritedrawn = pg.transform.rotate(self.sprite, 180)
        if vec == [-1, 0]:
            self.rect.move_ip(0, -self.speed)
            self.spritedrawn = pg.transform.rotate(self.sprite, -90)
        if vec == [1, 0]:
            self.rect.move_ip(0, self.speed)
            self.spritedrawn = pg.transform.rotate(self.sprite, 90)

    def move(self, vec):
        self.vec = vec
        if vec == [0, 1]:
            self.rect.move_ip(0, self.speed)
            self.spritedrawn = self.sprite
            self.y = self.y + self.speed
        if vec == [0, -1]:
            self.rect.move_ip(0, -self.speed)
            self.spritedrawn = pg.transform.rotate(self.sprite, 180)
            self.y = self.y - self.speed
        if vec == [-1, 0]:
            self.rect.move_ip(-self.speed, 0)
            self.spritedrawn = pg.transform.rotate(self.sprite, -90)
            self.x = self.x - self.speed
        if vec == [1, 0]:
            self.rect.move_ip(self.speed, 0)
            self.spritedrawn = pg.transform.rotate(self.sprite, 90)
            self.x = self.x + self.speed
