from itertools import filterfalse
import pygame as pg
from snake import Snake
from apple import Apple
from snake_body import Body
import random

pg.init()

screen = pg.display.set_mode((700, 700))
font = pg.font.SysFont("calibri", 20)

death = font.render("you lose", 1, "black")
restart = font.render('press "enter" to restart', 1, "black")

waiting = False
yammy = pg.mixer.Sound("yammy.wav")
btooom = pg.mixer.Sound("gnome.wav")
snake_alive = True
times = pg.time.Clock()
vec = [0, -1]
snake = Snake("sprites/snakehead.png", 334, 350, vec, 8)
x = snake.x
y = snake.y
apple = Apple("sprites/apple.png", random.randint(25, 700-25-32), random.randint(25, 700-25-32))
bodies = [Body("sprites/snakebody.png", 334, 350, vec, 8)]
for i in range(0, 7):
    bodies.append(Body("sprites/snakebody.png", bodies[i-1].x, bodies[i-1].y, bodies[i-1].vec, 8))
lenght = 7
running = True
while running:
    pg.display.flip()
    if snake_alive:
        times.tick(20)
        screen.fill("black")
        pg.draw.rect(screen, 'green', (25, 25, 650, 650), 5)
        screen.blit(apple.sprite, apple.rect)
        for i in range(len(bodies)-1, -1, -1):
            screen.blit(bodies[i].spritedrawn, bodies[i].rect)
        screen.blit(snake.spritedrawn, snake.rect)

        print(bodies[len(bodies)-1].x, bodies[len(bodies)-1].y)
        print(bodies[len(bodies)-2].x, bodies[len(bodies)-2].y, "\n")

        if pg.sprite.collide_rect(apple, snake):
            x = snake.x
            y = snake.y
            snake = Snake("sprites/snakehead.png", x, y, vec, 8)
            apple.teleport(random.randint(25, 700-25-32), random.randint(25, 700-25-32))
            lenght += 3
            bodies = [Body("sprites/snakebody.png", 334, 350, vec, 8)]
            for i in range(0, lenght):
                bodies.append(Body("sprites/snakebody.png", bodies[i-1].x, bodies[i-1].y, bodies[i-1].vec, 8))
            waiting = True
            yammy.play()
        if snake.rect.left < 25 or snake.rect.right > 675 or snake.rect.top < 25 or snake.rect.bottom > 675:
            btooom.play()
            snake_alive = False
        
        if waiting:
            waiting = False
        else:
            snake.move(vec)
            bodies[0].move(snake.vec, snake.x, snake.y)
            for i in range(len(bodies) - 1, 0, -1):
                bodies[i].move(bodies[i - 1].vec, bodies[i - 1].x, bodies[i - 1].y)
            

        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_w and vec != [0, 1]:
                    vec = [0, -1]
                elif event.key == pg.K_s and vec != [0, -1]:
                    vec = [0, 1]
                elif event.key == pg.K_a and vec != [1, 0]:
                    vec = [-1, 0]
                elif event.key == pg.K_d and vec != [-1, 0]:
                    vec = [1, 0]
            if event.type == pg.QUIT:
                running = False
    else:
        pg.draw.rect(screen, "green", (250, 300, 200, 100))
        screen.blit(death, (320, 310))
        screen.blit(restart, (255, 341))
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_RETURN:
                    snake = Snake("sprites/snakehead.png", 334, 250, vec, 8)
                    x = snake.x
                    y = snake.y
                    lenght = 7
                    bodies = [Body("sprites/snakebody.png", 334, 350, vec, 8)]
                    for i in range(0, 7):
                        bodies.append(Body("sprites/snakebody.png", bodies[i-1].x, bodies[i-1].y, bodies[i-1].vec, 8))
                    snake_alive = True
            if event.type == pg.QUIT:
                running = False
